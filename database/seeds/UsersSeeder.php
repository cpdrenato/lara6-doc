<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $count = User::all()->count();

        if (0 == $count) {
            $user = User::create([
                'name' => 'Renato',
                'email' => 'cpdrenato@gmail.com',
                'email_verified_at' => null,
                'password' => bcrypt('password'),
            ]);
        } else {
            echo 'Qtde: '.$count.' Records Inside Database!';
        }
    }
}
