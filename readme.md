<p align="center"><img src="2022-03-28_19-06.png" width="600"></p>

# Teste com documentacao de api

- php artisan scribe:generate
- php artisan migrate
- php artisan passport:install
- php artisan db:seed 

- http://localhost:8000/docs

# Dicas

> If you're using Sanctum with scribe, you have to set :

```config/scribe.php```

'use_csrf' => true, //default false

## A tool to automatically fix PHP code style
`composer require friendsofphp/php-cs-fixer`

## Fontes:

- https://github.com/knuckleswtf/scribe
- https://scribe.knuckles.wtf/laravel/getting-started
- https://memorandum.ewilan-riviere.com/development/frameworks/laravel/setup-dependencies/
